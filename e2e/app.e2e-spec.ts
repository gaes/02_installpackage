import { InstallpackagePage } from './app.po';

describe('installpackage App', () => {
  let page: InstallpackagePage;

  beforeEach(() => {
    page = new InstallpackagePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
